﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace guia4_Habilidades_GM181937
{
    public partial class ConversionLongitud : Form
    {
        public ConversionLongitud()
        {
            InitializeComponent();
        }

        private void txtMetros_KeyUp(object sender, KeyEventArgs e)
        {
            double metros, pies;

            try
            {
                //Vamos realizando la conversión por cada pulsación
                metros = Convert.ToDouble(txtMetros.Text);
                pies = metros * 3.281;
                pies = Math.Round(pies, 3);

                txtPies.Text = pies.ToString();
                lblFormula.Text = txtMetros.Text + " * 3.281";
            }
            catch (Exception)
            {

            }
        }

        private void txtMetros_KeyPress(object sender, KeyPressEventArgs e)
        {
            //Solo números
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) &&
                (e.KeyChar != '.'))
            {
                e.Handled = true;
            }

            // Solo punto decimal
            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
            }
        }

        private void txtPies_KeyPress(object sender, KeyPressEventArgs e)
        {
            //Solo números
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) &&
                (e.KeyChar != '.'))
            {
                e.Handled = true;
            }

            // Solo punto decimal
            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
            }
        }

        private void txtPies_KeyUp(object sender, KeyEventArgs e)
        {
            double metros, pies;

            try
            {
                //Vamos realizando la conversión por cada pulsación
                pies = Convert.ToDouble(txtPies.Text);
                metros = pies / 3.281;
                metros = Math.Round(metros, 3);

                txtMetros.Text = metros.ToString();
                lblFormula.Text = txtMetros.Text + " * 3.281";
            }
            catch (Exception)
            {

            }
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
