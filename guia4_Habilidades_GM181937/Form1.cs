﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace guia4_Habilidades_GM181937
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            customizarDiseño();
        }
        //Método que nos ayudara a ocultar el panel de submenú
        private void customizarDiseño()
        {
            panel1.Visible = false;
        }
        //Método que nos ayudara a esconder el submenú
        private void esconderSubmenu()
        {
            if (panel1.Visible == true)
            {
                panel1.Visible = false;
            }
        }
        //Método para mostrar el submenú
        private void mostrarSubmenu(Panel subMenu)
        {
            if (subMenu.Visible == false)
            {
                esconderSubmenu();
                subMenu.Visible = true;
            }
            else
            {
                subMenu.Visible = false;
            }
        }

        #region Conversiones
        private void btnConversiones_Click(object sender, EventArgs e)
        {
            mostrarSubmenu(panel1);
        }

        private void btnTemperatura_Click(object sender, EventArgs e)
        {
            abrirFormularios(new ConversionTemperatura());
            esconderSubmenu();
        }

        private void btnLongitud_Click(object sender, EventArgs e)
        {
            abrirFormularios(new ConversionLongitud());
            esconderSubmenu();
        }

        private void btnPeso_Click(object sender, EventArgs e)
        {
            abrirFormularios(new ConversionesPeso());
            esconderSubmenu();
        }
        #endregion

        private void btnFormulaCuadratica_Click(object sender, EventArgs e)
        {
            abrirFormularios(new FormEcuacionCuadratica());
            esconderSubmenu();
        }

        private void btnModificacionEjemplo_Click(object sender, EventArgs e)
        {
            abrirFormularios(new FormModificacionEjemploCinco());
            esconderSubmenu();
        }

        private void btnDescuentos_Click(object sender, EventArgs e)
        {
            abrirFormularios(new FormDescuentos());
            esconderSubmenu();
        }

        //Método para abrir los formularios en el panel
        private Form formularioAbierto = null;
        private void abrirFormularios(Form formulario)
        {
            if (formularioAbierto != null)
            {
                formularioAbierto.Close();
            }
            formularioAbierto = formulario;
            formulario.TopLevel = false; //Indicamos que el formulario se comportará como un control
            formulario.FormBorderStyle = FormBorderStyle.None;
            formulario.Dock = DockStyle.Fill;
            pnlContenedor.Controls.Add(formulario); //Agregamos el formulario a la lista de controles de pnlContenedor
            pnlContenedor.Tag = formulario; //Asociamos el formulario con nuestro panel
            //formulario.BringToFront(); //Por si usamos un logo en el panel
            formulario.Show(); //Mostramos el formulario
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
