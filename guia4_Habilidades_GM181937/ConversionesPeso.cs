﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace guia4_Habilidades_GM181937
{
    public partial class ConversionesPeso : Form
    {
        public ConversionesPeso()
        {
            InitializeComponent();
        }

        private void txtKilogramos_KeyUp(object sender, KeyEventArgs e)
        {
            double kilogramos, libras;

            try
            {
                //Vamos realizando la conversión por cada pulsación
                kilogramos = Convert.ToDouble(txtKilogramos.Text);
                libras = kilogramos * 2.205;
                libras = Math.Round(libras, 3);
                txtLibras.Text = libras.ToString();

                lblFormula.Text = txtKilogramos.Text + " * 2.205";
            }
            catch (Exception)
            {

            }
        }

        private void txtKilogramos_KeyPress(object sender, KeyPressEventArgs e)
        {
            //Solo números
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) &&
                (e.KeyChar != '.'))
            {
                e.Handled = true;
            }

            // Solo punto decimal
            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
            }
        }

        private void txtLibras_KeyUp(object sender, KeyEventArgs e)
        {
            double kilogramos, libras;

            try
            {
                //Vamos realizando la conversión por cada pulsación
                libras = Convert.ToDouble(txtLibras.Text);
                kilogramos = libras / 2.205;
                kilogramos = Math.Round(kilogramos, 3);
                txtKilogramos.Text = kilogramos.ToString();

                lblFormula.Text = txtKilogramos.Text + " * 2.205";
            }
            catch (Exception)
            {

            }
        }

        private void txtLibras_KeyPress(object sender, KeyPressEventArgs e)
        {
            //Solo números
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) &&
                (e.KeyChar != '.'))
            {
                e.Handled = true;
            }

            // Solo punto decimal
            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
            }
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
