﻿
namespace guia4_Habilidades_GM181937
{
    partial class ConversionTemperatura
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnSalir = new System.Windows.Forms.Button();
            this.lblTituloCtoF = new System.Windows.Forms.Label();
            this.lblGradosCelcius = new System.Windows.Forms.Label();
            this.txtCelcius = new System.Windows.Forms.TextBox();
            this.lblIgual = new System.Windows.Forms.Label();
            this.txtFaren = new System.Windows.Forms.TextBox();
            this.lblFarenheith = new System.Windows.Forms.Label();
            this.lblTituloFormula = new System.Windows.Forms.Label();
            this.lblFormula = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnSalir
            // 
            this.btnSalir.FlatAppearance.BorderSize = 0;
            this.btnSalir.FlatAppearance.MouseDownBackColor = System.Drawing.Color.MidnightBlue;
            this.btnSalir.FlatAppearance.MouseOverBackColor = System.Drawing.Color.MidnightBlue;
            this.btnSalir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSalir.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.btnSalir.ForeColor = System.Drawing.SystemColors.Control;
            this.btnSalir.Location = new System.Drawing.Point(12, 12);
            this.btnSalir.Name = "btnSalir";
            this.btnSalir.Size = new System.Drawing.Size(22, 23);
            this.btnSalir.TabIndex = 15;
            this.btnSalir.Text = "X";
            this.btnSalir.UseVisualStyleBackColor = true;
            this.btnSalir.Click += new System.EventHandler(this.btnSalir_Click);
            // 
            // lblTituloCtoF
            // 
            this.lblTituloCtoF.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lblTituloCtoF.AutoSize = true;
            this.lblTituloCtoF.Font = new System.Drawing.Font("Segoe UI Black", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.lblTituloCtoF.ForeColor = System.Drawing.Color.LightGray;
            this.lblTituloCtoF.Location = new System.Drawing.Point(280, 43);
            this.lblTituloCtoF.Name = "lblTituloCtoF";
            this.lblTituloCtoF.Size = new System.Drawing.Size(281, 37);
            this.lblTituloCtoF.TabIndex = 16;
            this.lblTituloCtoF.Text = "Celcius a Farenheith";
            // 
            // lblGradosCelcius
            // 
            this.lblGradosCelcius.AutoSize = true;
            this.lblGradosCelcius.Font = new System.Drawing.Font("Segoe UI Semilight", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lblGradosCelcius.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.lblGradosCelcius.Location = new System.Drawing.Point(163, 223);
            this.lblGradosCelcius.Name = "lblGradosCelcius";
            this.lblGradosCelcius.Size = new System.Drawing.Size(130, 25);
            this.lblGradosCelcius.TabIndex = 17;
            this.lblGradosCelcius.Text = "Grados celcius";
            // 
            // txtCelcius
            // 
            this.txtCelcius.Font = new System.Drawing.Font("Segoe UI Black", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.txtCelcius.Location = new System.Drawing.Point(141, 275);
            this.txtCelcius.Name = "txtCelcius";
            this.txtCelcius.Size = new System.Drawing.Size(172, 36);
            this.txtCelcius.TabIndex = 18;
            this.txtCelcius.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtCelcius.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCelcius_KeyPress);
            this.txtCelcius.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtCelcius_KeyUp);
            // 
            // lblIgual
            // 
            this.lblIgual.AutoSize = true;
            this.lblIgual.Font = new System.Drawing.Font("Segoe UI Semilight", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lblIgual.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.lblIgual.Location = new System.Drawing.Point(390, 223);
            this.lblIgual.Name = "lblIgual";
            this.lblIgual.Size = new System.Drawing.Size(50, 54);
            this.lblIgual.TabIndex = 19;
            this.lblIgual.Text = "=";
            // 
            // txtFaren
            // 
            this.txtFaren.Font = new System.Drawing.Font("Segoe UI Black", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.txtFaren.Location = new System.Drawing.Point(509, 275);
            this.txtFaren.Name = "txtFaren";
            this.txtFaren.Size = new System.Drawing.Size(181, 36);
            this.txtFaren.TabIndex = 21;
            this.txtFaren.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtFaren.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtFaren_KeyPress);
            this.txtFaren.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtFaren_KeyUp);
            // 
            // lblFarenheith
            // 
            this.lblFarenheith.AutoSize = true;
            this.lblFarenheith.Font = new System.Drawing.Font("Segoe UI Semilight", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lblFarenheith.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.lblFarenheith.Location = new System.Drawing.Point(520, 223);
            this.lblFarenheith.Name = "lblFarenheith";
            this.lblFarenheith.Size = new System.Drawing.Size(160, 25);
            this.lblFarenheith.TabIndex = 20;
            this.lblFarenheith.Text = "Grados Farenheith";
            // 
            // lblTituloFormula
            // 
            this.lblTituloFormula.AutoSize = true;
            this.lblTituloFormula.Font = new System.Drawing.Font("Segoe UI Semilight", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.lblTituloFormula.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.lblTituloFormula.Location = new System.Drawing.Point(141, 394);
            this.lblTituloFormula.Name = "lblTituloFormula";
            this.lblTituloFormula.Size = new System.Drawing.Size(90, 25);
            this.lblTituloFormula.TabIndex = 22;
            this.lblTituloFormula.Text = "Fórmula:";
            // 
            // lblFormula
            // 
            this.lblFormula.AutoSize = true;
            this.lblFormula.Font = new System.Drawing.Font("Bauhaus 93", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.lblFormula.ForeColor = System.Drawing.Color.DarkMagenta;
            this.lblFormula.Location = new System.Drawing.Point(141, 436);
            this.lblFormula.Name = "lblFormula";
            this.lblFormula.Size = new System.Drawing.Size(343, 30);
            this.lblFormula.TabIndex = 23;
            this.lblFormula.Text = "(0 °C × 9/5) + 32 = 32 °F";
            // 
            // ConversionTemperatura
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(21)))), ((int)(((byte)(32)))));
            this.ClientSize = new System.Drawing.Size(855, 632);
            this.Controls.Add(this.lblFormula);
            this.Controls.Add(this.lblTituloFormula);
            this.Controls.Add(this.txtFaren);
            this.Controls.Add(this.lblFarenheith);
            this.Controls.Add(this.lblIgual);
            this.Controls.Add(this.txtCelcius);
            this.Controls.Add(this.lblGradosCelcius);
            this.Controls.Add(this.lblTituloCtoF);
            this.Controls.Add(this.btnSalir);
            this.Name = "ConversionTemperatura";
            this.Text = "ConversionTemperatura";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnSalir;
        private System.Windows.Forms.Label lblTituloCtoF;
        private System.Windows.Forms.Label lblGradosCelcius;
        private System.Windows.Forms.TextBox txtCelcius;
        private System.Windows.Forms.Label lblIgual;
        private System.Windows.Forms.TextBox txtFaren;
        private System.Windows.Forms.Label lblFarenheith;
        private System.Windows.Forms.Label lblTituloFormula;
        private System.Windows.Forms.Label lblFormula;
    }
}