﻿
namespace guia4_Habilidades_GM181937
{
    partial class ConversionLongitud
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnSalir = new System.Windows.Forms.Button();
            this.lblFormula = new System.Windows.Forms.Label();
            this.lblTituloFormula = new System.Windows.Forms.Label();
            this.txtPies = new System.Windows.Forms.TextBox();
            this.lblPies = new System.Windows.Forms.Label();
            this.lblIgual = new System.Windows.Forms.Label();
            this.txtMetros = new System.Windows.Forms.TextBox();
            this.lblMetros = new System.Windows.Forms.Label();
            this.lblTituloMetrosAPies = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnSalir
            // 
            this.btnSalir.FlatAppearance.BorderSize = 0;
            this.btnSalir.FlatAppearance.MouseDownBackColor = System.Drawing.Color.MidnightBlue;
            this.btnSalir.FlatAppearance.MouseOverBackColor = System.Drawing.Color.MidnightBlue;
            this.btnSalir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSalir.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.btnSalir.ForeColor = System.Drawing.SystemColors.Control;
            this.btnSalir.Location = new System.Drawing.Point(12, 12);
            this.btnSalir.Name = "btnSalir";
            this.btnSalir.Size = new System.Drawing.Size(22, 23);
            this.btnSalir.TabIndex = 16;
            this.btnSalir.Text = "X";
            this.btnSalir.UseVisualStyleBackColor = true;
            this.btnSalir.Click += new System.EventHandler(this.btnSalir_Click);
            // 
            // lblFormula
            // 
            this.lblFormula.AutoSize = true;
            this.lblFormula.Font = new System.Drawing.Font("Bauhaus 93", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.lblFormula.ForeColor = System.Drawing.Color.DarkMagenta;
            this.lblFormula.Location = new System.Drawing.Point(145, 466);
            this.lblFormula.Name = "lblFormula";
            this.lblFormula.Size = new System.Drawing.Size(545, 30);
            this.lblFormula.TabIndex = 31;
            this.lblFormula.Text = "Multiplica el valor de longitud por 3.281";
            // 
            // lblTituloFormula
            // 
            this.lblTituloFormula.AutoSize = true;
            this.lblTituloFormula.Font = new System.Drawing.Font("Segoe UI Semilight", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.lblTituloFormula.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.lblTituloFormula.Location = new System.Drawing.Point(145, 424);
            this.lblTituloFormula.Name = "lblTituloFormula";
            this.lblTituloFormula.Size = new System.Drawing.Size(90, 25);
            this.lblTituloFormula.TabIndex = 30;
            this.lblTituloFormula.Text = "Fórmula:";
            // 
            // txtPies
            // 
            this.txtPies.Font = new System.Drawing.Font("Segoe UI Black", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.txtPies.Location = new System.Drawing.Point(516, 288);
            this.txtPies.Name = "txtPies";
            this.txtPies.Size = new System.Drawing.Size(181, 36);
            this.txtPies.TabIndex = 29;
            this.txtPies.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtPies.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPies_KeyPress);
            this.txtPies.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtPies_KeyUp);
            // 
            // lblPies
            // 
            this.lblPies.AutoSize = true;
            this.lblPies.Font = new System.Drawing.Font("Segoe UI Semilight", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lblPies.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.lblPies.Location = new System.Drawing.Point(582, 236);
            this.lblPies.Name = "lblPies";
            this.lblPies.Size = new System.Drawing.Size(48, 25);
            this.lblPies.TabIndex = 28;
            this.lblPies.Text = "Pies:";
            // 
            // lblIgual
            // 
            this.lblIgual.AutoSize = true;
            this.lblIgual.Font = new System.Drawing.Font("Segoe UI Semilight", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lblIgual.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.lblIgual.Location = new System.Drawing.Point(397, 236);
            this.lblIgual.Name = "lblIgual";
            this.lblIgual.Size = new System.Drawing.Size(50, 54);
            this.lblIgual.TabIndex = 27;
            this.lblIgual.Text = "=";
            // 
            // txtMetros
            // 
            this.txtMetros.Font = new System.Drawing.Font("Segoe UI Black", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.txtMetros.Location = new System.Drawing.Point(148, 288);
            this.txtMetros.Name = "txtMetros";
            this.txtMetros.Size = new System.Drawing.Size(172, 36);
            this.txtMetros.TabIndex = 26;
            this.txtMetros.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtMetros.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtMetros_KeyPress);
            this.txtMetros.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtMetros_KeyUp);
            // 
            // lblMetros
            // 
            this.lblMetros.AutoSize = true;
            this.lblMetros.Font = new System.Drawing.Font("Segoe UI Semilight", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lblMetros.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.lblMetros.Location = new System.Drawing.Point(189, 236);
            this.lblMetros.Name = "lblMetros";
            this.lblMetros.Size = new System.Drawing.Size(73, 25);
            this.lblMetros.TabIndex = 25;
            this.lblMetros.Text = "Metros:";
            // 
            // lblTituloMetrosAPies
            // 
            this.lblTituloMetrosAPies.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lblTituloMetrosAPies.AutoSize = true;
            this.lblTituloMetrosAPies.Font = new System.Drawing.Font("Segoe UI Black", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.lblTituloMetrosAPies.ForeColor = System.Drawing.Color.LightGray;
            this.lblTituloMetrosAPies.Location = new System.Drawing.Point(316, 73);
            this.lblTituloMetrosAPies.Name = "lblTituloMetrosAPies";
            this.lblTituloMetrosAPies.Size = new System.Drawing.Size(196, 37);
            this.lblTituloMetrosAPies.TabIndex = 24;
            this.lblTituloMetrosAPies.Text = "Metros a pies";
            // 
            // ConversionLongitud
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(21)))), ((int)(((byte)(32)))));
            this.ClientSize = new System.Drawing.Size(855, 632);
            this.Controls.Add(this.lblFormula);
            this.Controls.Add(this.lblTituloFormula);
            this.Controls.Add(this.txtPies);
            this.Controls.Add(this.lblPies);
            this.Controls.Add(this.lblIgual);
            this.Controls.Add(this.txtMetros);
            this.Controls.Add(this.lblMetros);
            this.Controls.Add(this.lblTituloMetrosAPies);
            this.Controls.Add(this.btnSalir);
            this.Name = "ConversionLongitud";
            this.Text = "ConversionLongitud";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnSalir;
        private System.Windows.Forms.Label lblFormula;
        private System.Windows.Forms.Label lblTituloFormula;
        private System.Windows.Forms.TextBox txtPies;
        private System.Windows.Forms.Label lblPies;
        private System.Windows.Forms.Label lblIgual;
        private System.Windows.Forms.TextBox txtMetros;
        private System.Windows.Forms.Label lblMetros;
        private System.Windows.Forms.Label lblTituloMetrosAPies;
    }
}