﻿
namespace guia4_Habilidades_GM181937
{
    partial class FormDescuentos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblTituloDescuentos = new System.Windows.Forms.Label();
            this.lblNombre = new System.Windows.Forms.Label();
            this.txtNombres = new System.Windows.Forms.TextBox();
            this.lblApellidos = new System.Windows.Forms.Label();
            this.txtApellidos = new System.Windows.Forms.TextBox();
            this.lblSalario = new System.Windows.Forms.Label();
            this.txtSalario = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.btnCalcular = new System.Windows.Forms.Button();
            this.lstResultados = new System.Windows.Forms.ListBox();
            this.btnSalir = new System.Windows.Forms.Button();
            this.grbCargo = new System.Windows.Forms.GroupBox();
            this.rdbSecretaria = new System.Windows.Forms.RadioButton();
            this.rdbSubgerente = new System.Windows.Forms.RadioButton();
            this.rdbGerente = new System.Windows.Forms.RadioButton();
            this.lblResultado = new System.Windows.Forms.Label();
            this.grbCargo.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblTituloDescuentos
            // 
            this.lblTituloDescuentos.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lblTituloDescuentos.AutoSize = true;
            this.lblTituloDescuentos.Font = new System.Drawing.Font("Segoe UI Black", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.lblTituloDescuentos.ForeColor = System.Drawing.Color.LightGray;
            this.lblTituloDescuentos.Location = new System.Drawing.Point(330, 31);
            this.lblTituloDescuentos.Name = "lblTituloDescuentos";
            this.lblTituloDescuentos.Size = new System.Drawing.Size(168, 37);
            this.lblTituloDescuentos.TabIndex = 0;
            this.lblTituloDescuentos.Text = "Descuentos";
            // 
            // lblNombre
            // 
            this.lblNombre.AutoSize = true;
            this.lblNombre.Font = new System.Drawing.Font("Segoe UI Semilight", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lblNombre.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.lblNombre.Location = new System.Drawing.Point(27, 133);
            this.lblNombre.Name = "lblNombre";
            this.lblNombre.Size = new System.Drawing.Size(92, 25);
            this.lblNombre.TabIndex = 1;
            this.lblNombre.Text = "Nombres:";
            // 
            // txtNombres
            // 
            this.txtNombres.Location = new System.Drawing.Point(193, 138);
            this.txtNombres.Name = "txtNombres";
            this.txtNombres.Size = new System.Drawing.Size(172, 23);
            this.txtNombres.TabIndex = 2;
            // 
            // lblApellidos
            // 
            this.lblApellidos.AutoSize = true;
            this.lblApellidos.Font = new System.Drawing.Font("Segoe UI Semilight", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lblApellidos.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.lblApellidos.Location = new System.Drawing.Point(27, 194);
            this.lblApellidos.Name = "lblApellidos";
            this.lblApellidos.Size = new System.Drawing.Size(91, 25);
            this.lblApellidos.TabIndex = 3;
            this.lblApellidos.Text = "Apellidos:";
            // 
            // txtApellidos
            // 
            this.txtApellidos.Location = new System.Drawing.Point(193, 199);
            this.txtApellidos.Name = "txtApellidos";
            this.txtApellidos.Size = new System.Drawing.Size(172, 23);
            this.txtApellidos.TabIndex = 4;
            // 
            // lblSalario
            // 
            this.lblSalario.AutoSize = true;
            this.lblSalario.Font = new System.Drawing.Font("Segoe UI Semilight", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lblSalario.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.lblSalario.Location = new System.Drawing.Point(27, 261);
            this.lblSalario.Name = "lblSalario";
            this.lblSalario.Size = new System.Drawing.Size(120, 25);
            this.lblSalario.TabIndex = 5;
            this.lblSalario.Text = "Salario bruto:";
            // 
            // txtSalario
            // 
            this.txtSalario.Location = new System.Drawing.Point(193, 266);
            this.txtSalario.Name = "txtSalario";
            this.txtSalario.Size = new System.Drawing.Size(172, 23);
            this.txtSalario.TabIndex = 6;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label3.Location = new System.Drawing.Point(27, 92);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(103, 14);
            this.label3.TabIndex = 7;
            this.label3.Text = "Datos personales:";
            // 
            // btnCalcular
            // 
            this.btnCalcular.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnCalcular.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(11)))), ((int)(((byte)(7)))), ((int)(((byte)(11)))));
            this.btnCalcular.FlatAppearance.BorderSize = 0;
            this.btnCalcular.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCalcular.Font = new System.Drawing.Font("Segoe UI Black", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btnCalcular.ForeColor = System.Drawing.Color.Gainsboro;
            this.btnCalcular.Location = new System.Drawing.Point(347, 333);
            this.btnCalcular.Name = "btnCalcular";
            this.btnCalcular.Size = new System.Drawing.Size(134, 38);
            this.btnCalcular.TabIndex = 12;
            this.btnCalcular.Text = "Calcular";
            this.btnCalcular.UseVisualStyleBackColor = false;
            this.btnCalcular.Click += new System.EventHandler(this.btnCalcular_Click);
            // 
            // lstResultados
            // 
            this.lstResultados.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lstResultados.FormattingEnabled = true;
            this.lstResultados.ItemHeight = 14;
            this.lstResultados.Location = new System.Drawing.Point(27, 408);
            this.lstResultados.Name = "lstResultados";
            this.lstResultados.Size = new System.Drawing.Size(801, 200);
            this.lstResultados.TabIndex = 13;
            // 
            // btnSalir
            // 
            this.btnSalir.FlatAppearance.BorderSize = 0;
            this.btnSalir.FlatAppearance.MouseDownBackColor = System.Drawing.Color.MidnightBlue;
            this.btnSalir.FlatAppearance.MouseOverBackColor = System.Drawing.Color.MidnightBlue;
            this.btnSalir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSalir.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.btnSalir.ForeColor = System.Drawing.SystemColors.Control;
            this.btnSalir.Location = new System.Drawing.Point(27, 12);
            this.btnSalir.Name = "btnSalir";
            this.btnSalir.Size = new System.Drawing.Size(22, 23);
            this.btnSalir.TabIndex = 14;
            this.btnSalir.Text = "X";
            this.btnSalir.UseVisualStyleBackColor = true;
            this.btnSalir.Click += new System.EventHandler(this.btnSalir_Click);
            // 
            // grbCargo
            // 
            this.grbCargo.Controls.Add(this.rdbSecretaria);
            this.grbCargo.Controls.Add(this.rdbSubgerente);
            this.grbCargo.Controls.Add(this.rdbGerente);
            this.grbCargo.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.grbCargo.Location = new System.Drawing.Point(481, 101);
            this.grbCargo.Name = "grbCargo";
            this.grbCargo.Size = new System.Drawing.Size(327, 188);
            this.grbCargo.TabIndex = 15;
            this.grbCargo.TabStop = false;
            this.grbCargo.Text = "Cargo que desempeña";
            // 
            // rdbSecretaria
            // 
            this.rdbSecretaria.AutoSize = true;
            this.rdbSecretaria.Font = new System.Drawing.Font("Segoe UI Semilight", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.rdbSecretaria.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.rdbSecretaria.Location = new System.Drawing.Point(18, 155);
            this.rdbSecretaria.Name = "rdbSecretaria";
            this.rdbSecretaria.Size = new System.Drawing.Size(94, 25);
            this.rdbSecretaria.TabIndex = 14;
            this.rdbSecretaria.TabStop = true;
            this.rdbSecretaria.Text = "Secretaria";
            this.rdbSecretaria.UseVisualStyleBackColor = true;
            this.rdbSecretaria.CheckedChanged += new System.EventHandler(this.rdbSecretaria_CheckedChanged_1);
            // 
            // rdbSubgerente
            // 
            this.rdbSubgerente.AutoSize = true;
            this.rdbSubgerente.Font = new System.Drawing.Font("Segoe UI Semilight", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.rdbSubgerente.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.rdbSubgerente.Location = new System.Drawing.Point(18, 88);
            this.rdbSubgerente.Name = "rdbSubgerente";
            this.rdbSubgerente.Size = new System.Drawing.Size(108, 25);
            this.rdbSubgerente.TabIndex = 13;
            this.rdbSubgerente.TabStop = true;
            this.rdbSubgerente.Text = "SubGerente";
            this.rdbSubgerente.UseVisualStyleBackColor = true;
            this.rdbSubgerente.CheckedChanged += new System.EventHandler(this.rdbSubgerente_CheckedChanged_1);
            // 
            // rdbGerente
            // 
            this.rdbGerente.AutoSize = true;
            this.rdbGerente.Font = new System.Drawing.Font("Segoe UI Semilight", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.rdbGerente.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.rdbGerente.Location = new System.Drawing.Point(18, 22);
            this.rdbGerente.Name = "rdbGerente";
            this.rdbGerente.Size = new System.Drawing.Size(82, 25);
            this.rdbGerente.TabIndex = 12;
            this.rdbGerente.TabStop = true;
            this.rdbGerente.Text = "Gerente";
            this.rdbGerente.UseVisualStyleBackColor = true;
            this.rdbGerente.CheckedChanged += new System.EventHandler(this.rdbGerente_CheckedChanged_1);
            // 
            // lblResultado
            // 
            this.lblResultado.AutoSize = true;
            this.lblResultado.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.lblResultado.Location = new System.Drawing.Point(27, 391);
            this.lblResultado.Name = "lblResultado";
            this.lblResultado.Size = new System.Drawing.Size(145, 14);
            this.lblResultado.TabIndex = 16;
            this.lblResultado.Text = "Resultados del descuento:";
            // 
            // FormDescuentos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(21)))), ((int)(((byte)(32)))));
            this.ClientSize = new System.Drawing.Size(855, 632);
            this.Controls.Add(this.lblResultado);
            this.Controls.Add(this.grbCargo);
            this.Controls.Add(this.btnSalir);
            this.Controls.Add(this.lstResultados);
            this.Controls.Add(this.btnCalcular);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtSalario);
            this.Controls.Add(this.lblSalario);
            this.Controls.Add(this.txtApellidos);
            this.Controls.Add(this.lblApellidos);
            this.Controls.Add(this.txtNombres);
            this.Controls.Add(this.lblNombre);
            this.Controls.Add(this.lblTituloDescuentos);
            this.Name = "FormDescuentos";
            this.Text = "FormDescuentos";
            this.grbCargo.ResumeLayout(false);
            this.grbCargo.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblTituloDescuentos;
        private System.Windows.Forms.Label lblNombre;
        private System.Windows.Forms.TextBox txtNombres;
        private System.Windows.Forms.Label lblApellidos;
        private System.Windows.Forms.TextBox txtApellidos;
        private System.Windows.Forms.Label lblSalario;
        private System.Windows.Forms.TextBox txtSalario;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnCalcular;
        private System.Windows.Forms.ListBox lstResultados;
        private System.Windows.Forms.Button btnSalir;
        private System.Windows.Forms.GroupBox grbCargo;
        private System.Windows.Forms.RadioButton rdbSecretaria;
        private System.Windows.Forms.RadioButton rdbSubgerente;
        private System.Windows.Forms.RadioButton rdbGerente;
        private System.Windows.Forms.Label lblResultado;
    }
}