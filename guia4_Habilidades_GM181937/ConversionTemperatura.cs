﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace guia4_Habilidades_GM181937
{
    public partial class ConversionTemperatura : Form
    {
        public ConversionTemperatura()
        {
            InitializeComponent();
        }

        private void txtCelcius_KeyPress(object sender, KeyPressEventArgs e)
        {
            //Solo números
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) &&
                (e.KeyChar != '.'))
            {
                e.Handled = true;
            }

            // Solo punto decimal
            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
            }
        }

        private void txtCelcius_KeyUp(object sender, KeyEventArgs e)
        {
            double cel, far;

            try
            {
                //Vamos realizando la conversión por cada pulsación
                cel = Convert.ToDouble(txtCelcius.Text);
                far = cel * 9.0 / 5.0 + 32;
                far = Math.Round(far, 2);
                txtFaren.Text = far.ToString();

                lblFormula.Text = "(" + txtCelcius.Text + " °C × 9/5) + 32 = " + far + "F°";
            }
            catch (Exception)
            {

            }
        }

        private void txtFaren_KeyPress(object sender, KeyPressEventArgs e)
        {
            //Solo números
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) &&
                (e.KeyChar != '.'))
            {
                e.Handled = true;
            }

            // Solo punto decimal
            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
            }
        }

        private void txtFaren_KeyUp(object sender, KeyEventArgs e)
        {
            double cel, far;

            try
            {
                //Vamos realizando la conversión por cada pulsación
                far = Convert.ToDouble(txtFaren.Text);
                cel = (far - 32.0) * (5.0 / 9.0);
                cel = Math.Round(cel, 2);
                txtCelcius.Text = cel.ToString();

                lblFormula.Text = "(" + cel + " °C × 9/5) + 32 = " + far + "F°";
            }
            catch (Exception)
            {

            }
        }
        private void btnSalir_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
