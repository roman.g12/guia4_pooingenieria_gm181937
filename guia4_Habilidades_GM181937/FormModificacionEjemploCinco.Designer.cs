﻿
namespace guia4_Habilidades_GM181937
{
    partial class FormModificacionEjemploCinco
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnSalir = new System.Windows.Forms.Button();
            this.lblTituloCalculos = new System.Windows.Forms.Label();
            this.lblIngresarValor = new System.Windows.Forms.Label();
            this.txtIngresarValor = new System.Windows.Forms.TextBox();
            this.lstArreglo = new System.Windows.Forms.ListBox();
            this.grbOperaciones = new System.Windows.Forms.GroupBox();
            this.btnCalcular = new System.Windows.Forms.Button();
            this.txtCalculo4 = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtCalculo3 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtCalculo2 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtCalculo1 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.grbOperaciones.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnSalir
            // 
            this.btnSalir.FlatAppearance.BorderSize = 0;
            this.btnSalir.FlatAppearance.MouseDownBackColor = System.Drawing.Color.MidnightBlue;
            this.btnSalir.FlatAppearance.MouseOverBackColor = System.Drawing.Color.MidnightBlue;
            this.btnSalir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSalir.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.btnSalir.ForeColor = System.Drawing.SystemColors.Control;
            this.btnSalir.Location = new System.Drawing.Point(12, 12);
            this.btnSalir.Name = "btnSalir";
            this.btnSalir.Size = new System.Drawing.Size(22, 23);
            this.btnSalir.TabIndex = 15;
            this.btnSalir.Text = "X";
            this.btnSalir.UseVisualStyleBackColor = true;
            this.btnSalir.Click += new System.EventHandler(this.btnSalir_Click);
            // 
            // lblTituloCalculos
            // 
            this.lblTituloCalculos.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lblTituloCalculos.AutoSize = true;
            this.lblTituloCalculos.Font = new System.Drawing.Font("Segoe UI Black", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.lblTituloCalculos.ForeColor = System.Drawing.Color.LightGray;
            this.lblTituloCalculos.Location = new System.Drawing.Point(277, 34);
            this.lblTituloCalculos.Name = "lblTituloCalculos";
            this.lblTituloCalculos.Size = new System.Drawing.Size(297, 37);
            this.lblTituloCalculos.TabIndex = 16;
            this.lblTituloCalculos.Text = "Cálculos con arreglos";
            // 
            // lblIngresarValor
            // 
            this.lblIngresarValor.AutoSize = true;
            this.lblIngresarValor.Font = new System.Drawing.Font("Segoe UI Semilight", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lblIngresarValor.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.lblIngresarValor.Location = new System.Drawing.Point(56, 108);
            this.lblIngresarValor.Name = "lblIngresarValor";
            this.lblIngresarValor.Size = new System.Drawing.Size(224, 25);
            this.lblIngresarValor.TabIndex = 17;
            this.lblIngresarValor.Text = "Ingrese un valor al arreglo";
            // 
            // txtIngresarValor
            // 
            this.txtIngresarValor.Location = new System.Drawing.Point(286, 108);
            this.txtIngresarValor.Multiline = true;
            this.txtIngresarValor.Name = "txtIngresarValor";
            this.txtIngresarValor.Size = new System.Drawing.Size(100, 33);
            this.txtIngresarValor.TabIndex = 18;
            this.txtIngresarValor.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtIngresarValor.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtIngresarValor_KeyPress);
            // 
            // lstArreglo
            // 
            this.lstArreglo.FormattingEnabled = true;
            this.lstArreglo.ItemHeight = 14;
            this.lstArreglo.Location = new System.Drawing.Point(56, 202);
            this.lstArreglo.Name = "lstArreglo";
            this.lstArreglo.Size = new System.Drawing.Size(148, 340);
            this.lstArreglo.TabIndex = 19;
            // 
            // grbOperaciones
            // 
            this.grbOperaciones.Controls.Add(this.btnCalcular);
            this.grbOperaciones.Controls.Add(this.txtCalculo4);
            this.grbOperaciones.Controls.Add(this.label4);
            this.grbOperaciones.Controls.Add(this.txtCalculo3);
            this.grbOperaciones.Controls.Add(this.label3);
            this.grbOperaciones.Controls.Add(this.txtCalculo2);
            this.grbOperaciones.Controls.Add(this.label2);
            this.grbOperaciones.Controls.Add(this.txtCalculo1);
            this.grbOperaciones.Controls.Add(this.label1);
            this.grbOperaciones.Font = new System.Drawing.Font("Segoe UI Semilight", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.grbOperaciones.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.grbOperaciones.Location = new System.Drawing.Point(319, 202);
            this.grbOperaciones.Name = "grbOperaciones";
            this.grbOperaciones.Size = new System.Drawing.Size(414, 340);
            this.grbOperaciones.TabIndex = 20;
            this.grbOperaciones.TabStop = false;
            this.grbOperaciones.Text = "OPERACIONES CON ARREGLO";
            // 
            // btnCalcular
            // 
            this.btnCalcular.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCalcular.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(11)))), ((int)(((byte)(7)))), ((int)(((byte)(11)))));
            this.btnCalcular.FlatAppearance.BorderSize = 0;
            this.btnCalcular.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCalcular.Font = new System.Drawing.Font("Segoe UI Black", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btnCalcular.ForeColor = System.Drawing.Color.Gainsboro;
            this.btnCalcular.Location = new System.Drawing.Point(263, 287);
            this.btnCalcular.Name = "btnCalcular";
            this.btnCalcular.Size = new System.Drawing.Size(134, 38);
            this.btnCalcular.TabIndex = 28;
            this.btnCalcular.Text = "Calcular";
            this.btnCalcular.UseVisualStyleBackColor = false;
            this.btnCalcular.Click += new System.EventHandler(this.btnCalcular_Click);
            // 
            // txtCalculo4
            // 
            this.txtCalculo4.Enabled = false;
            this.txtCalculo4.Location = new System.Drawing.Point(297, 236);
            this.txtCalculo4.Multiline = true;
            this.txtCalculo4.Name = "txtCalculo4";
            this.txtCalculo4.Size = new System.Drawing.Size(100, 33);
            this.txtCalculo4.TabIndex = 27;
            this.txtCalculo4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 236);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(232, 21);
            this.label4.TabIndex = 26;
            this.label4.Text = "Mayor de los pares positivos";
            // 
            // txtCalculo3
            // 
            this.txtCalculo3.Enabled = false;
            this.txtCalculo3.Location = new System.Drawing.Point(297, 166);
            this.txtCalculo3.Multiline = true;
            this.txtCalculo3.Name = "txtCalculo3";
            this.txtCalculo3.Size = new System.Drawing.Size(100, 33);
            this.txtCalculo3.TabIndex = 25;
            this.txtCalculo3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 169);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(249, 21);
            this.label3.TabIndex = 24;
            this.label3.Text = "Promedio de impares positivos";
            // 
            // txtCalculo2
            // 
            this.txtCalculo2.Enabled = false;
            this.txtCalculo2.Location = new System.Drawing.Point(297, 101);
            this.txtCalculo2.Multiline = true;
            this.txtCalculo2.Name = "txtCalculo2";
            this.txtCalculo2.Size = new System.Drawing.Size(100, 33);
            this.txtCalculo2.TabIndex = 23;
            this.txtCalculo2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 104);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(264, 21);
            this.label2.TabIndex = 22;
            this.label2.Text = "Porcentaje de ceros en el arreglo";
            // 
            // txtCalculo1
            // 
            this.txtCalculo1.Enabled = false;
            this.txtCalculo1.Location = new System.Drawing.Point(297, 39);
            this.txtCalculo1.Multiline = true;
            this.txtCalculo1.Name = "txtCalculo1";
            this.txtCalculo1.Size = new System.Drawing.Size(100, 33);
            this.txtCalculo1.TabIndex = 21;
            this.txtCalculo1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 42);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(275, 21);
            this.label1.TabIndex = 0;
            this.label1.Text = "Número mayor de pares negativos";
            // 
            // FormModificacionEjemploCinco
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(21)))), ((int)(((byte)(32)))));
            this.ClientSize = new System.Drawing.Size(855, 632);
            this.Controls.Add(this.grbOperaciones);
            this.Controls.Add(this.lstArreglo);
            this.Controls.Add(this.txtIngresarValor);
            this.Controls.Add(this.lblIngresarValor);
            this.Controls.Add(this.lblTituloCalculos);
            this.Controls.Add(this.btnSalir);
            this.Name = "FormModificacionEjemploCinco";
            this.Text = "FormModificacionEjemploCinco";
            this.grbOperaciones.ResumeLayout(false);
            this.grbOperaciones.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnSalir;
        private System.Windows.Forms.Label lblTituloCalculos;
        private System.Windows.Forms.Label lblIngresarValor;
        private System.Windows.Forms.TextBox txtIngresarValor;
        private System.Windows.Forms.ListBox lstArreglo;
        private System.Windows.Forms.GroupBox grbOperaciones;
        private System.Windows.Forms.TextBox txtCalculo4;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtCalculo3;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtCalculo2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtCalculo1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnCalcular;
    }
}