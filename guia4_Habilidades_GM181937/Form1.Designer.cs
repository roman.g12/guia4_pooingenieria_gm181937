﻿
namespace guia4_Habilidades_GM181937
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlMenuLateral = new System.Windows.Forms.Panel();
            this.btnModificacionEjemplo = new System.Windows.Forms.Button();
            this.btnFormulaCuadratica = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnPeso = new System.Windows.Forms.Button();
            this.btnLongitud = new System.Windows.Forms.Button();
            this.btnTemperatura = new System.Windows.Forms.Button();
            this.btnConversiones = new System.Windows.Forms.Button();
            this.btnDescuentos = new System.Windows.Forms.Button();
            this.pnlTitulo = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.pnlContenedor = new System.Windows.Forms.Panel();
            this.btnSalir = new System.Windows.Forms.Button();
            this.pnlMenuLateral.SuspendLayout();
            this.panel1.SuspendLayout();
            this.pnlTitulo.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlMenuLateral
            // 
            this.pnlMenuLateral.AutoScroll = true;
            this.pnlMenuLateral.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(11)))), ((int)(((byte)(7)))), ((int)(((byte)(11)))));
            this.pnlMenuLateral.Controls.Add(this.btnSalir);
            this.pnlMenuLateral.Controls.Add(this.btnModificacionEjemplo);
            this.pnlMenuLateral.Controls.Add(this.btnFormulaCuadratica);
            this.pnlMenuLateral.Controls.Add(this.panel1);
            this.pnlMenuLateral.Controls.Add(this.btnConversiones);
            this.pnlMenuLateral.Controls.Add(this.btnDescuentos);
            this.pnlMenuLateral.Controls.Add(this.pnlTitulo);
            this.pnlMenuLateral.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnlMenuLateral.Location = new System.Drawing.Point(0, 0);
            this.pnlMenuLateral.Name = "pnlMenuLateral";
            this.pnlMenuLateral.Size = new System.Drawing.Size(218, 632);
            this.pnlMenuLateral.TabIndex = 0;
            // 
            // btnModificacionEjemplo
            // 
            this.btnModificacionEjemplo.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnModificacionEjemplo.FlatAppearance.BorderSize = 0;
            this.btnModificacionEjemplo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnModificacionEjemplo.ForeColor = System.Drawing.Color.Gainsboro;
            this.btnModificacionEjemplo.Location = new System.Drawing.Point(0, 358);
            this.btnModificacionEjemplo.Name = "btnModificacionEjemplo";
            this.btnModificacionEjemplo.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.btnModificacionEjemplo.Size = new System.Drawing.Size(218, 45);
            this.btnModificacionEjemplo.TabIndex = 5;
            this.btnModificacionEjemplo.Text = "Modificación ejemplo 5";
            this.btnModificacionEjemplo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnModificacionEjemplo.UseVisualStyleBackColor = true;
            this.btnModificacionEjemplo.Click += new System.EventHandler(this.btnModificacionEjemplo_Click);
            // 
            // btnFormulaCuadratica
            // 
            this.btnFormulaCuadratica.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnFormulaCuadratica.FlatAppearance.BorderSize = 0;
            this.btnFormulaCuadratica.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFormulaCuadratica.ForeColor = System.Drawing.Color.Gainsboro;
            this.btnFormulaCuadratica.Location = new System.Drawing.Point(0, 313);
            this.btnFormulaCuadratica.Name = "btnFormulaCuadratica";
            this.btnFormulaCuadratica.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.btnFormulaCuadratica.Size = new System.Drawing.Size(218, 45);
            this.btnFormulaCuadratica.TabIndex = 4;
            this.btnFormulaCuadratica.Text = "Fórmula Cuadrática";
            this.btnFormulaCuadratica.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnFormulaCuadratica.UseVisualStyleBackColor = true;
            this.btnFormulaCuadratica.Click += new System.EventHandler(this.btnFormulaCuadratica_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(32)))), ((int)(((byte)(39)))));
            this.panel1.Controls.Add(this.btnPeso);
            this.panel1.Controls.Add(this.btnLongitud);
            this.panel1.Controls.Add(this.btnTemperatura);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 190);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(218, 123);
            this.panel1.TabIndex = 3;
            // 
            // btnPeso
            // 
            this.btnPeso.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnPeso.FlatAppearance.BorderSize = 0;
            this.btnPeso.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPeso.ForeColor = System.Drawing.Color.LightGray;
            this.btnPeso.Location = new System.Drawing.Point(0, 80);
            this.btnPeso.Name = "btnPeso";
            this.btnPeso.Padding = new System.Windows.Forms.Padding(35, 0, 0, 0);
            this.btnPeso.Size = new System.Drawing.Size(218, 40);
            this.btnPeso.TabIndex = 2;
            this.btnPeso.Text = "Peso";
            this.btnPeso.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPeso.UseVisualStyleBackColor = true;
            this.btnPeso.Click += new System.EventHandler(this.btnPeso_Click);
            // 
            // btnLongitud
            // 
            this.btnLongitud.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnLongitud.FlatAppearance.BorderSize = 0;
            this.btnLongitud.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLongitud.ForeColor = System.Drawing.Color.LightGray;
            this.btnLongitud.Location = new System.Drawing.Point(0, 40);
            this.btnLongitud.Name = "btnLongitud";
            this.btnLongitud.Padding = new System.Windows.Forms.Padding(35, 0, 0, 0);
            this.btnLongitud.Size = new System.Drawing.Size(218, 40);
            this.btnLongitud.TabIndex = 1;
            this.btnLongitud.Text = "Longitud";
            this.btnLongitud.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnLongitud.UseVisualStyleBackColor = true;
            this.btnLongitud.Click += new System.EventHandler(this.btnLongitud_Click);
            // 
            // btnTemperatura
            // 
            this.btnTemperatura.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnTemperatura.FlatAppearance.BorderSize = 0;
            this.btnTemperatura.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTemperatura.ForeColor = System.Drawing.Color.LightGray;
            this.btnTemperatura.Location = new System.Drawing.Point(0, 0);
            this.btnTemperatura.Name = "btnTemperatura";
            this.btnTemperatura.Padding = new System.Windows.Forms.Padding(35, 0, 0, 0);
            this.btnTemperatura.Size = new System.Drawing.Size(218, 40);
            this.btnTemperatura.TabIndex = 0;
            this.btnTemperatura.Text = "Temperatura";
            this.btnTemperatura.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnTemperatura.UseVisualStyleBackColor = true;
            this.btnTemperatura.Click += new System.EventHandler(this.btnTemperatura_Click);
            // 
            // btnConversiones
            // 
            this.btnConversiones.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnConversiones.FlatAppearance.BorderSize = 0;
            this.btnConversiones.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnConversiones.ForeColor = System.Drawing.Color.Gainsboro;
            this.btnConversiones.Location = new System.Drawing.Point(0, 145);
            this.btnConversiones.Name = "btnConversiones";
            this.btnConversiones.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.btnConversiones.Size = new System.Drawing.Size(218, 45);
            this.btnConversiones.TabIndex = 2;
            this.btnConversiones.Text = "Realizar conversiones";
            this.btnConversiones.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnConversiones.UseVisualStyleBackColor = true;
            this.btnConversiones.Click += new System.EventHandler(this.btnConversiones_Click);
            // 
            // btnDescuentos
            // 
            this.btnDescuentos.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnDescuentos.FlatAppearance.BorderSize = 0;
            this.btnDescuentos.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDescuentos.ForeColor = System.Drawing.Color.Gainsboro;
            this.btnDescuentos.Location = new System.Drawing.Point(0, 100);
            this.btnDescuentos.Name = "btnDescuentos";
            this.btnDescuentos.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.btnDescuentos.Size = new System.Drawing.Size(218, 45);
            this.btnDescuentos.TabIndex = 1;
            this.btnDescuentos.Text = "Descuentos";
            this.btnDescuentos.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnDescuentos.UseVisualStyleBackColor = true;
            this.btnDescuentos.Click += new System.EventHandler(this.btnDescuentos_Click);
            // 
            // pnlTitulo
            // 
            this.pnlTitulo.Controls.Add(this.label1);
            this.pnlTitulo.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlTitulo.Location = new System.Drawing.Point(0, 0);
            this.pnlTitulo.Name = "pnlTitulo";
            this.pnlTitulo.Size = new System.Drawing.Size(218, 100);
            this.pnlTitulo.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI Black", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(28, 37);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(159, 25);
            this.label1.TabIndex = 0;
            this.label1.Text = "Proyecto Guía 2";
            // 
            // pnlContenedor
            // 
            this.pnlContenedor.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(21)))), ((int)(((byte)(32)))));
            this.pnlContenedor.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlContenedor.Location = new System.Drawing.Point(218, 0);
            this.pnlContenedor.Name = "pnlContenedor";
            this.pnlContenedor.Size = new System.Drawing.Size(855, 632);
            this.pnlContenedor.TabIndex = 1;
            // 
            // btnSalir
            // 
            this.btnSalir.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnSalir.FlatAppearance.BorderSize = 0;
            this.btnSalir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSalir.ForeColor = System.Drawing.Color.Gainsboro;
            this.btnSalir.Location = new System.Drawing.Point(0, 587);
            this.btnSalir.Name = "btnSalir";
            this.btnSalir.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.btnSalir.Size = new System.Drawing.Size(218, 45);
            this.btnSalir.TabIndex = 6;
            this.btnSalir.Text = "SALIR";
            this.btnSalir.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSalir.UseVisualStyleBackColor = true;
            this.btnSalir.Click += new System.EventHandler(this.btnSalir_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1073, 632);
            this.Controls.Add(this.pnlContenedor);
            this.Controls.Add(this.pnlMenuLateral);
            this.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.MaximumSize = new System.Drawing.Size(1089, 671);
            this.MinimumSize = new System.Drawing.Size(950, 600);
            this.Name = "Form1";
            this.Text = "Proyecto guía 2";
            this.pnlMenuLateral.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.pnlTitulo.ResumeLayout(false);
            this.pnlTitulo.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlMenuLateral;
        private System.Windows.Forms.Button btnConversiones;
        private System.Windows.Forms.Button btnDescuentos;
        private System.Windows.Forms.Panel pnlTitulo;
        private System.Windows.Forms.Button btnModificacionEjemplo;
        private System.Windows.Forms.Button btnFormulaCuadratica;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnPeso;
        private System.Windows.Forms.Button btnLongitud;
        private System.Windows.Forms.Button btnTemperatura;
        private System.Windows.Forms.Panel pnlContenedor;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnSalir;
    }
}

