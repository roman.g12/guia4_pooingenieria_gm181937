﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace guia4_Habilidades_GM181937
{
    public partial class FormEcuacionCuadratica : Form
    {
        public FormEcuacionCuadratica()
        {
            InitializeComponent();
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnCalcular_Click(object sender, EventArgs e)
        {
            //Guardamos en las variables el valor de las txt
            double x1, x2;
            float valorA = float.Parse(txtA.Text);
            float valorB = float.Parse(txtB.Text);
            float valorC = float.Parse(txtC.Text);

            //utilizamos formula para X1 y X2
            x1 = ((-1) * valorB + Math.Sqrt(((valorB * valorB) - (4 * valorA * valorC)))) / (2 * valorA);
            x2 = ((-1) * valorB - Math.Sqrt(((valorB * valorB) - (4 * valorA * valorC)))) / (2 * valorA);

            lblX1.Text = x1.ToString();
            lblX2.Text = x2.ToString();
        }
    }
}
