﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace guia4_Habilidades_GM181937
{
    public partial class FormDescuentos : Form
    {
        public FormDescuentos()
        {
            InitializeComponent();
        }

        //Variables globales
        double desc = 0;
        string cargo = null;

        private void btnSalir_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnCalcular_Click(object sender, EventArgs e)
        {
            try
            {
                if (desc == 0)
                {
                    grbCargo.Focus();
                    throw new Exception("Por favor seleccione un cargo");
                }
                if (string.IsNullOrEmpty(txtNombres.Text))
                {
                    txtNombres.Focus();
                    throw new Exception("Por favor ingrese su nombre");
                }
                if (string.IsNullOrEmpty(txtSalario.Text))
                {
                    txtSalario.Focus();
                    throw new Exception("Por favor ingrese su salario");
                }
                double salarioBase = double.Parse(txtSalario.Text);
                if (salarioBase < 0)
                {
                    txtSalario.Focus();
                    throw new Exception("Ingrese un salario positivo");
                }
                double salarioDes = salarioBase - (salarioBase * desc);
                lblResultado.Text = "Resultados del descuento";
                lstResultados.Items.Clear();
                lstResultados.Visible = true;
                lstResultados.Items.Add("Nombre: " + txtNombres.Text + " " + txtApellidos.Text);
                lstResultados.Items.Add("Salario Base: $" + salarioBase.ToString());
                lstResultados.Items.Add("Cargo: " + cargo);
                lstResultados.Items.Add("Descuento porcentual aplicado: " + (desc * 100).ToString() + "%");
                lstResultados.Items.Add("Descuento aplicado: $" + (salarioBase * desc).ToString());
                lstResultados.Items.Add("Salario con Descuento: $" + salarioDes.ToString());
            }
            catch (FormatException formatEx)
            {
                MessageBox.Show("¡¡ERROR!! " + formatEx + "Ingrese un salario valido ", "Alerta");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Alerta");
            }
        }

        private void rdbGerente_CheckedChanged_1(object sender, EventArgs e)
        {
            this.desc = 0.20;
            cargo = "Gerente";
        }

        private void rdbSubgerente_CheckedChanged_1(object sender, EventArgs e)
        {
            this.desc = 0.15;
            cargo = "SubGerente";
        }

        private void rdbSecretaria_CheckedChanged_1(object sender, EventArgs e)
        {
            this.desc = 0.05;
            cargo = "Secretaria";
        }
    }
}
