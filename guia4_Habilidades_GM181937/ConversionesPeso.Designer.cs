﻿
namespace guia4_Habilidades_GM181937
{
    partial class ConversionesPeso
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnSalir = new System.Windows.Forms.Button();
            this.lblFormula = new System.Windows.Forms.Label();
            this.lblTituloFormula = new System.Windows.Forms.Label();
            this.txtLibras = new System.Windows.Forms.TextBox();
            this.lblLibras = new System.Windows.Forms.Label();
            this.lblIgual = new System.Windows.Forms.Label();
            this.txtKilogramos = new System.Windows.Forms.TextBox();
            this.lblKilogramos = new System.Windows.Forms.Label();
            this.lblTituloKilogramosALibras = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnSalir
            // 
            this.btnSalir.FlatAppearance.BorderSize = 0;
            this.btnSalir.FlatAppearance.MouseDownBackColor = System.Drawing.Color.MidnightBlue;
            this.btnSalir.FlatAppearance.MouseOverBackColor = System.Drawing.Color.MidnightBlue;
            this.btnSalir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSalir.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.btnSalir.ForeColor = System.Drawing.SystemColors.Control;
            this.btnSalir.Location = new System.Drawing.Point(12, 12);
            this.btnSalir.Name = "btnSalir";
            this.btnSalir.Size = new System.Drawing.Size(22, 23);
            this.btnSalir.TabIndex = 16;
            this.btnSalir.Text = "X";
            this.btnSalir.UseVisualStyleBackColor = true;
            this.btnSalir.Click += new System.EventHandler(this.btnSalir_Click);
            // 
            // lblFormula
            // 
            this.lblFormula.AutoSize = true;
            this.lblFormula.Font = new System.Drawing.Font("Bauhaus 93", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.lblFormula.ForeColor = System.Drawing.Color.DarkMagenta;
            this.lblFormula.Location = new System.Drawing.Point(154, 461);
            this.lblFormula.Name = "lblFormula";
            this.lblFormula.Size = new System.Drawing.Size(501, 30);
            this.lblFormula.TabIndex = 31;
            this.lblFormula.Text = "multiplica el valor de masa por 2.205";
            // 
            // lblTituloFormula
            // 
            this.lblTituloFormula.AutoSize = true;
            this.lblTituloFormula.Font = new System.Drawing.Font("Segoe UI Semilight", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.lblTituloFormula.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.lblTituloFormula.Location = new System.Drawing.Point(154, 419);
            this.lblTituloFormula.Name = "lblTituloFormula";
            this.lblTituloFormula.Size = new System.Drawing.Size(90, 25);
            this.lblTituloFormula.TabIndex = 30;
            this.lblTituloFormula.Text = "Fórmula:";
            // 
            // txtLibras
            // 
            this.txtLibras.Font = new System.Drawing.Font("Segoe UI Black", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.txtLibras.Location = new System.Drawing.Point(522, 288);
            this.txtLibras.Name = "txtLibras";
            this.txtLibras.Size = new System.Drawing.Size(181, 36);
            this.txtLibras.TabIndex = 29;
            this.txtLibras.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtLibras.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtLibras_KeyPress);
            this.txtLibras.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtLibras_KeyUp);
            // 
            // lblLibras
            // 
            this.lblLibras.AutoSize = true;
            this.lblLibras.Font = new System.Drawing.Font("Segoe UI Semilight", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lblLibras.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.lblLibras.Location = new System.Drawing.Point(583, 236);
            this.lblLibras.Name = "lblLibras";
            this.lblLibras.Size = new System.Drawing.Size(60, 25);
            this.lblLibras.TabIndex = 28;
            this.lblLibras.Text = "Libras";
            // 
            // lblIgual
            // 
            this.lblIgual.AutoSize = true;
            this.lblIgual.Font = new System.Drawing.Font("Segoe UI Semilight", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lblIgual.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.lblIgual.Location = new System.Drawing.Point(403, 236);
            this.lblIgual.Name = "lblIgual";
            this.lblIgual.Size = new System.Drawing.Size(50, 54);
            this.lblIgual.TabIndex = 27;
            this.lblIgual.Text = "=";
            // 
            // txtKilogramos
            // 
            this.txtKilogramos.Font = new System.Drawing.Font("Segoe UI Black", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.txtKilogramos.Location = new System.Drawing.Point(154, 288);
            this.txtKilogramos.Name = "txtKilogramos";
            this.txtKilogramos.Size = new System.Drawing.Size(172, 36);
            this.txtKilogramos.TabIndex = 26;
            this.txtKilogramos.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtKilogramos.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtKilogramos_KeyPress);
            this.txtKilogramos.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtKilogramos_KeyUp);
            // 
            // lblKilogramos
            // 
            this.lblKilogramos.AutoSize = true;
            this.lblKilogramos.Font = new System.Drawing.Font("Segoe UI Semilight", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lblKilogramos.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.lblKilogramos.Location = new System.Drawing.Point(188, 236);
            this.lblKilogramos.Name = "lblKilogramos";
            this.lblKilogramos.Size = new System.Drawing.Size(104, 25);
            this.lblKilogramos.TabIndex = 25;
            this.lblKilogramos.Text = "Kilogramos";
            // 
            // lblTituloKilogramosALibras
            // 
            this.lblTituloKilogramosALibras.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lblTituloKilogramosALibras.AutoSize = true;
            this.lblTituloKilogramosALibras.Font = new System.Drawing.Font("Segoe UI Black", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.lblTituloKilogramosALibras.ForeColor = System.Drawing.Color.LightGray;
            this.lblTituloKilogramosALibras.Location = new System.Drawing.Point(281, 77);
            this.lblTituloKilogramosALibras.Name = "lblTituloKilogramosALibras";
            this.lblTituloKilogramosALibras.Size = new System.Drawing.Size(273, 37);
            this.lblTituloKilogramosALibras.TabIndex = 24;
            this.lblTituloKilogramosALibras.Text = "Kilogramos a libras";
            // 
            // ConversionesPeso
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(21)))), ((int)(((byte)(32)))));
            this.ClientSize = new System.Drawing.Size(855, 632);
            this.Controls.Add(this.lblFormula);
            this.Controls.Add(this.lblTituloFormula);
            this.Controls.Add(this.txtLibras);
            this.Controls.Add(this.lblLibras);
            this.Controls.Add(this.lblIgual);
            this.Controls.Add(this.txtKilogramos);
            this.Controls.Add(this.lblKilogramos);
            this.Controls.Add(this.lblTituloKilogramosALibras);
            this.Controls.Add(this.btnSalir);
            this.Name = "ConversionesPeso";
            this.Text = "ConversionesPeso";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnSalir;
        private System.Windows.Forms.Label lblFormula;
        private System.Windows.Forms.Label lblTituloFormula;
        private System.Windows.Forms.TextBox txtLibras;
        private System.Windows.Forms.Label lblLibras;
        private System.Windows.Forms.Label lblIgual;
        private System.Windows.Forms.TextBox txtKilogramos;
        private System.Windows.Forms.Label lblKilogramos;
        private System.Windows.Forms.Label lblTituloKilogramosALibras;
    }
}