﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace guia4_Habilidades_GM181937
{
    public partial class FormModificacionEjemploCinco : Form
    {
        public FormModificacionEjemploCinco()
        {
            InitializeComponent();
        }

        //Evento para poder ingresar datos al arreglo
        private void txtIngresarValor_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter))
            {
                lstArreglo.Items.Add(txtIngresarValor.Text);
                txtIngresarValor.Clear();
                txtIngresarValor.Focus();
            }
        }

        //Realizando los cálculos
        private void btnCalcular_Click(object sender, EventArgs e)
        {
            //Calcular el mayor de los pares negativos
            int mayorneg = -1000;
            for (int i = 0; i < lstArreglo.Items.Count; i++)
            {
                string valor = lstArreglo.Items[i].ToString();
                int numero = int.Parse(valor);

                if (numero < 0 && numero % 2 == 0)
                {
                    if (numero > mayorneg)
                    {
                        mayorneg = numero;
                        txtCalculo1.Text = mayorneg.ToString();
                    }
                }
            }

            //Calcular porcentaje de ceros
            double cantidadNumeros = lstArreglo.Items.Count;
            double cantidadCeros = 0, porcentaje = 0;

            for (int i = 0; i < lstArreglo.Items.Count; i++)
            {
                string valor = lstArreglo.Items[i].ToString();
                int numero = int.Parse(valor);

                if (numero == 0)
                {
                    cantidadCeros = cantidadCeros + 1;
                }
            }
            porcentaje = (cantidadCeros / cantidadNumeros) * 100;
            txtCalculo2.Text = porcentaje.ToString() + "%";

            //Obtener promedio de impares positivos
            double prom;
            double cantidadImpares = 0, suma = 0;

            for (int i = 0; i < lstArreglo.Items.Count; i++)
            {
                string valor = lstArreglo.Items[i].ToString();
                int numero = int.Parse(valor);

                if (numero > 0 && numero % 2 != 0)
                {
                    suma = suma + numero;
                    cantidadImpares = cantidadImpares + 1;
                }
            }
            prom = suma / cantidadImpares;
            prom = Math.Round(prom, 2);
            txtCalculo3.Text = prom.ToString();

            //Calcular mayor de pares positivos
            int mayor = 0;

            for (int i = 0; i < lstArreglo.Items.Count; i++)
            {
                string valor = lstArreglo.Items[i].ToString();
                int numero = int.Parse(valor);

                if (numero > 0 && numero % 2 == 0)
                {
                    if (numero > mayor)
                    {
                        mayor = numero;
                    }
                }
            }
            txtCalculo4.Text = mayor.ToString();
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
