﻿
namespace guia4_Habilidades_GM181937
{
    partial class FormEcuacionCuadratica
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblTituloEcuacion = new System.Windows.Forms.Label();
            this.btnSalir = new System.Windows.Forms.Button();
            this.lblA = new System.Windows.Forms.Label();
            this.txtA = new System.Windows.Forms.TextBox();
            this.lblB = new System.Windows.Forms.Label();
            this.txtB = new System.Windows.Forms.TextBox();
            this.txtC = new System.Windows.Forms.TextBox();
            this.lblC = new System.Windows.Forms.Label();
            this.lblTituloX1 = new System.Windows.Forms.Label();
            this.lblTituloX2 = new System.Windows.Forms.Label();
            this.lblX1 = new System.Windows.Forms.Label();
            this.lblX2 = new System.Windows.Forms.Label();
            this.btnCalcular = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblTituloEcuacion
            // 
            this.lblTituloEcuacion.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lblTituloEcuacion.AutoSize = true;
            this.lblTituloEcuacion.Font = new System.Drawing.Font("Segoe UI Black", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.lblTituloEcuacion.ForeColor = System.Drawing.Color.LightGray;
            this.lblTituloEcuacion.Location = new System.Drawing.Point(280, 67);
            this.lblTituloEcuacion.Name = "lblTituloEcuacion";
            this.lblTituloEcuacion.Size = new System.Drawing.Size(276, 37);
            this.lblTituloEcuacion.TabIndex = 1;
            this.lblTituloEcuacion.Text = "Ecuación cuadrática";
            // 
            // btnSalir
            // 
            this.btnSalir.FlatAppearance.BorderSize = 0;
            this.btnSalir.FlatAppearance.MouseDownBackColor = System.Drawing.Color.MidnightBlue;
            this.btnSalir.FlatAppearance.MouseOverBackColor = System.Drawing.Color.MidnightBlue;
            this.btnSalir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSalir.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.btnSalir.ForeColor = System.Drawing.SystemColors.Control;
            this.btnSalir.Location = new System.Drawing.Point(12, 12);
            this.btnSalir.Name = "btnSalir";
            this.btnSalir.Size = new System.Drawing.Size(22, 23);
            this.btnSalir.TabIndex = 15;
            this.btnSalir.Text = "X";
            this.btnSalir.UseVisualStyleBackColor = true;
            this.btnSalir.Click += new System.EventHandler(this.btnSalir_Click);
            // 
            // lblA
            // 
            this.lblA.AutoSize = true;
            this.lblA.Font = new System.Drawing.Font("Segoe UI Semilight", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lblA.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.lblA.Location = new System.Drawing.Point(126, 193);
            this.lblA.Name = "lblA";
            this.lblA.Size = new System.Drawing.Size(26, 25);
            this.lblA.TabIndex = 16;
            this.lblA.Text = "a:";
            // 
            // txtA
            // 
            this.txtA.Location = new System.Drawing.Point(158, 198);
            this.txtA.Name = "txtA";
            this.txtA.Size = new System.Drawing.Size(60, 23);
            this.txtA.TabIndex = 17;
            // 
            // lblB
            // 
            this.lblB.AutoSize = true;
            this.lblB.Font = new System.Drawing.Font("Segoe UI Semilight", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lblB.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.lblB.Location = new System.Drawing.Point(377, 193);
            this.lblB.Name = "lblB";
            this.lblB.Size = new System.Drawing.Size(27, 25);
            this.lblB.TabIndex = 18;
            this.lblB.Text = "b:";
            // 
            // txtB
            // 
            this.txtB.Location = new System.Drawing.Point(410, 198);
            this.txtB.Name = "txtB";
            this.txtB.Size = new System.Drawing.Size(60, 23);
            this.txtB.TabIndex = 19;
            // 
            // txtC
            // 
            this.txtC.Location = new System.Drawing.Point(661, 198);
            this.txtC.Name = "txtC";
            this.txtC.Size = new System.Drawing.Size(60, 23);
            this.txtC.TabIndex = 21;
            // 
            // lblC
            // 
            this.lblC.AutoSize = true;
            this.lblC.Font = new System.Drawing.Font("Segoe UI Semilight", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lblC.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.lblC.Location = new System.Drawing.Point(628, 193);
            this.lblC.Name = "lblC";
            this.lblC.Size = new System.Drawing.Size(25, 25);
            this.lblC.TabIndex = 20;
            this.lblC.Text = "c:";
            // 
            // lblTituloX1
            // 
            this.lblTituloX1.AutoSize = true;
            this.lblTituloX1.Font = new System.Drawing.Font("Segoe UI Semilight", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.lblTituloX1.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.lblTituloX1.Location = new System.Drawing.Point(109, 360);
            this.lblTituloX1.Name = "lblTituloX1";
            this.lblTituloX1.Size = new System.Drawing.Size(52, 37);
            this.lblTituloX1.TabIndex = 22;
            this.lblTituloX1.Text = "X1:";
            // 
            // lblTituloX2
            // 
            this.lblTituloX2.AutoSize = true;
            this.lblTituloX2.Font = new System.Drawing.Font("Segoe UI Semilight", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.lblTituloX2.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.lblTituloX2.Location = new System.Drawing.Point(105, 448);
            this.lblTituloX2.Name = "lblTituloX2";
            this.lblTituloX2.Size = new System.Drawing.Size(56, 37);
            this.lblTituloX2.TabIndex = 23;
            this.lblTituloX2.Text = "X2:";
            // 
            // lblX1
            // 
            this.lblX1.AutoSize = true;
            this.lblX1.Font = new System.Drawing.Font("Segoe UI Semilight", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.lblX1.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.lblX1.Location = new System.Drawing.Point(167, 360);
            this.lblX1.Name = "lblX1";
            this.lblX1.Size = new System.Drawing.Size(34, 37);
            this.lblX1.TabIndex = 24;
            this.lblX1.Text = "#";
            // 
            // lblX2
            // 
            this.lblX2.AutoSize = true;
            this.lblX2.Font = new System.Drawing.Font("Segoe UI Semilight", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.lblX2.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.lblX2.Location = new System.Drawing.Point(167, 448);
            this.lblX2.Name = "lblX2";
            this.lblX2.Size = new System.Drawing.Size(34, 37);
            this.lblX2.TabIndex = 25;
            this.lblX2.Text = "#";
            // 
            // btnCalcular
            // 
            this.btnCalcular.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCalcular.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(11)))), ((int)(((byte)(7)))), ((int)(((byte)(11)))));
            this.btnCalcular.FlatAppearance.BorderSize = 0;
            this.btnCalcular.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCalcular.Font = new System.Drawing.Font("Segoe UI Black", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btnCalcular.ForeColor = System.Drawing.Color.Gainsboro;
            this.btnCalcular.Location = new System.Drawing.Point(587, 447);
            this.btnCalcular.Name = "btnCalcular";
            this.btnCalcular.Size = new System.Drawing.Size(134, 38);
            this.btnCalcular.TabIndex = 26;
            this.btnCalcular.Text = "Calcular";
            this.btnCalcular.UseVisualStyleBackColor = false;
            this.btnCalcular.Click += new System.EventHandler(this.btnCalcular_Click);
            // 
            // FormEcuacionCuadratica
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(21)))), ((int)(((byte)(32)))));
            this.ClientSize = new System.Drawing.Size(855, 632);
            this.Controls.Add(this.btnCalcular);
            this.Controls.Add(this.lblX2);
            this.Controls.Add(this.lblX1);
            this.Controls.Add(this.lblTituloX2);
            this.Controls.Add(this.lblTituloX1);
            this.Controls.Add(this.txtC);
            this.Controls.Add(this.lblC);
            this.Controls.Add(this.txtB);
            this.Controls.Add(this.lblB);
            this.Controls.Add(this.txtA);
            this.Controls.Add(this.lblA);
            this.Controls.Add(this.btnSalir);
            this.Controls.Add(this.lblTituloEcuacion);
            this.Name = "FormEcuacionCuadratica";
            this.Text = "FormEcuacionCuadratica";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblTituloEcuacion;
        private System.Windows.Forms.Button btnSalir;
        private System.Windows.Forms.Label lblA;
        private System.Windows.Forms.TextBox txtA;
        private System.Windows.Forms.Label lblB;
        private System.Windows.Forms.TextBox txtB;
        private System.Windows.Forms.TextBox txtC;
        private System.Windows.Forms.Label lblC;
        private System.Windows.Forms.Label lblTituloX1;
        private System.Windows.Forms.Label lblTituloX2;
        private System.Windows.Forms.Label lblX1;
        private System.Windows.Forms.Label lblX2;
        private System.Windows.Forms.Button btnCalcular;
    }
}